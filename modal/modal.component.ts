import { Component, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { ModalProps, ModalState, ModalBehavior } from '@beyonder/common/modal';

@Component({
  selector: 'beyonder-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements ModalProps, ModalState, ModalBehavior {

  @ViewChild('self')
  private self: ElementRef;

  @Input()
  inClassname = '';

  @Input()
  outClassname = '';

  @Output()
  onClose = new EventEmitter<string>();

  isOpen = true;

  close(): void {
    const triggerOnClose = () => {
      this.onClose.emit('beyonder-modal-closed');
      this.self.nativeElement.removeEventListener('animationend', triggerOnClose);
    };

    this.isOpen = false;

    if (this.outClassname.length === 0) {
      triggerOnClose();
    } else {
      this.self.nativeElement.addEventListener('animationend', triggerOnClose);
    }
  }

}
